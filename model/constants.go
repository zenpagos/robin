package model

const (
	PaymentType           = "payment"
	PaymentApprovedAction = "payment.approved"

	WebhookNotificationDataFormat = `{"id": "%s"}`
)
