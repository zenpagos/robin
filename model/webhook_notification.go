// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	"encoding/json"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type WebhookNotification struct {
	ID        uuid.UUID       `json:"id" sql:"type:varchar(255);primary_key;"`
	CreatedAt int64           `json:"created_at" sql:"type:int;not null"`
	Type      string          `json:"type" sql:"type:varchar(255);not null"`
	Action    string          `json:"action" sql:"type:varchar(255);not null"`
	Data      json.RawMessage `json:"data" sql:"type:varchar(255);not null"`
	AccountID uuid.UUID       `json:"account_id" sql:"type:varchar(255);not null"`
}

func (wn WebhookNotification) Validate() error {
	return validation.ValidateStruct(&wn,
		validation.Field(&wn.Type, validation.Required),
		validation.Field(&wn.Action, validation.Required),
		validation.Field(&wn.Data, validation.Required),
		validation.Field(&wn.AccountID, is.UUIDv4, validation.Required),
	)
}
