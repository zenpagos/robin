package config

const (
	PaymentApprovedTopic = "notification.payment.approved"
	UserRegisteredTopic  = "notification.user.registered"

	DefaultCompanyName      = "Zenpagos"
	DefaultEmailAddressFrom = "no-reply@zenpagos.com"
)
