package config

type ServiceConfiguration struct {
	Port           int    `mapstructure:"port"`
	Prod           bool   `mapstructure:"prod"`
	SendGridApiKey string `mapstructure:"send_grid_api_key"`
}
