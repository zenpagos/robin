package config

type KafkaConfiguration struct {
	Host          string `mapstructure:"host"`
	Port          int    `mapstructure:"port"`
	ConsumerGroup string `mapstructure:"consumer_group"`
}
