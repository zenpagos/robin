package schema

type PaymentApprovedSchema struct {
	PaymentID         string `json:"payment_id"`
	PaymentMethod     string `json:"payment_method"`
	PaymentMethodType string `json:"payment_method_type"`
	Description       string `json:"description"`
	PayerName         string `json:"payer_name"`
	PayerEmail        string `json:"payer_email"`
	Currency          string `json:"currency"`
	PaidAmount        int64  `json:"paid_amount"`
	Installments      int64  `json:"installments"`
	InstallmentAmount int64  `json:"installment_amount"`
	AccountID         string `json:"account_id"`
}
