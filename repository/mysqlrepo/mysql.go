package mysqlrepo

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/config"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/tools"
)

const DatabaseDialect = "mysql"

func NewMysqlClient(c config.Configuration, log *logrus.Logger) (*gorm.DB, error) {
	connectionUri := fmt.Sprintf(
		"%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		c.Database.Username,
		c.Database.Password,
		c.Database.Host,
		c.Database.Database,
	)

	db, err := gorm.Open(DatabaseDialect, connectionUri)
	if err != nil {
		return nil, err
	}

	db.SetLogger(log)

	if c.Service.Prod == false {
		db.LogMode(true)
	}

	db.Callback().
		Create().
		Before("gorm:create").
		After("gorm:before_create").
		Register("generateUUID:before_create", generateUUID)

	db.Callback().
		Create().
		Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)

	db.Callback().
		Update().
		Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)

	db.AutoMigrate(
		&model.Webhook{},
		&model.WebhookNotification{},
	)

	return db, nil
}

func generateUUID(scope *gorm.Scope) {
	if !scope.HasError() {
		id := uuid.NewV4()

		if IDField, ok := scope.FieldByName("ID"); ok {
			if IDField.IsBlank {
				IDField.Set(id)
			}
		}
	}
}

func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		now := tools.NewUnixTime()

		if createdAtField, ok := scope.FieldByName("CreatedAt"); ok {
			if createdAtField.IsBlank {
				createdAtField.Set(now)
			}
		}
	}
}

func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		now := tools.NewUnixTime()
		scope.SetColumn("UpdatedAt", now)
	}
}
