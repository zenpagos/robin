package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/repository"
)

type WebhookRepository struct {
	DB *gorm.DB
}

func NewWebhookRepository(db *gorm.DB) repository.WebhookRepositoryInterface {
	return WebhookRepository{
		DB: db,
	}
}

func (r WebhookRepository) Insert(c *model.Webhook) error {
	return r.DB.Create(&c).Error
}

func (r WebhookRepository) FindByAccountID(accountID uuid.UUID) ([]model.Webhook, error) {
	var ws []model.Webhook
	if err := r.DB.Where("account_id = ?", accountID.String()).
		Find(&ws).Error; err != nil {
		return nil, err
	}

	return ws, nil
}
