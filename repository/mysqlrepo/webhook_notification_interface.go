package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/repository"
)

type WebhookNotificationRepository struct {
	DB *gorm.DB
}

func NewWebhookNotificationRepository(db *gorm.DB) repository.WebhookNotificationRepositoryInterface {
	return WebhookNotificationRepository{
		DB: db,
	}
}

func (r WebhookNotificationRepository) Insert(c *model.WebhookNotification) error {
	return r.DB.Create(&c).Error
}
