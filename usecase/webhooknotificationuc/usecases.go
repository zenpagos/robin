// Package registration represents the concrete implementation of WebhookNotificationCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package webhooknotificationuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/repository"
	"gitlab.com/zenpagos/robin/usecase"
)

type WebhookNotificationUseCase struct {
	log                           *logrus.Logger
	WebhookNotificationRepository repository.WebhookNotificationRepositoryInterface
}

func NewWebhookNotificationCase(
	log *logrus.Logger,
	wr repository.WebhookNotificationRepositoryInterface,
) usecase.WebhookNotificationUseCaseInterface {
	return WebhookNotificationUseCase{
		log:                           log,
		WebhookNotificationRepository: wr,
	}
}

func (uc WebhookNotificationUseCase) CreateWebhookNotification(w *model.WebhookNotification) error {
	if err := w.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.WebhookNotificationRepository.Insert(w); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}
