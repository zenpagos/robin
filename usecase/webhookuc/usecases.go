// Package registration represents the concrete implementation of WebhookCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package webhookuc

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/repository"
	"gitlab.com/zenpagos/robin/usecase"
)

type WebhookUseCase struct {
	log               *logrus.Logger
	WebhookRepository repository.WebhookRepositoryInterface
}

func NewWebhookCase(
	log *logrus.Logger,
	wr repository.WebhookRepositoryInterface,
) usecase.WebhookUseCaseInterface {
	return WebhookUseCase{
		log:               log,
		WebhookRepository: wr,
	}
}

func (uc WebhookUseCase) CreateWebhook(w *model.Webhook) error {
	if err := w.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.WebhookRepository.Insert(w); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc WebhookUseCase) ListWebhooks(accountID uuid.UUID) ([]model.Webhook, error) {
	return uc.WebhookRepository.FindByAccountID(accountID)
}
