// Package registration represents the concrete implementation of NotificatorCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package notificatoruc

import (
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/usecase"
	"gitlab.com/zenpagos/tools"
)

type NotificatorUseCase struct {
	log         *logrus.Logger
	restyClient *resty.Client
}

func NewNotificatorCase(log *logrus.Logger) usecase.NotificatorUseCaseInterface {
	return NotificatorUseCase{
		restyClient: resty.New(),
	}
}

func (uc NotificatorUseCase) SendNotification(ws []model.Webhook, wn model.WebhookNotification) error {
	for _, w := range ws {
		request := uc.restyClient.R().
			SetHeader("Content-Type", "application/json").
			SetBody(&wn)

		if w.AuthorizationHeader != nil {
			request.SetHeader("Authorization", tools.StringValueOrBlank(w.AuthorizationHeader))
		}

		res, err := request.Post(w.URL)
		if err != nil {
			uc.log.WithError(err).Error()
			return err
		}

		uc.log.WithFields(logrus.Fields{
			"status_code": res.StatusCode(),
			"received_at": res.ReceivedAt(),
			"trace_info":  res.Request.TraceInfo(),
			"response":    res.String(),
		}).Info()
	}

	return nil
}
