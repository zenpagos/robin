// Package registration represents the concrete implementation of MailingUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package mailinguc

import (
	"fmt"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/config"
	"gitlab.com/zenpagos/robin/schema"
	"gitlab.com/zenpagos/robin/usecase"
	"gitlab.com/zenpagos/robin/wrapper/wsendgrid"
)

type MailingUseCase struct {
	log       *logrus.Logger
	WSendGrid *wsendgrid.SendGridWrapper
}

func NewMailingUseCase(
	log *logrus.Logger,
	wsg *wsendgrid.SendGridWrapper,
) usecase.MailingUseCaseInterface {
	return MailingUseCase{
		log:       log,
		WSendGrid: wsg,
	}
}

func (uc MailingUseCase) NotifyToPayerWhenAPaymentIsReceived(s *schema.PaymentApprovedSchema) error {
	from := mail.NewEmail(config.DefaultCompanyName, config.DefaultEmailAddressFrom)
	subject := fmt.Sprintf("Hey %s recibimos tu pago", s.PayerName)
	to := mail.NewEmail(s.PayerName, s.PayerEmail)
	plainTextContent := fmt.Sprintf("Gracias por pagar %d", s.PaidAmount)
	htmlContent := fmt.Sprintf("<strong>Gracias por pagar %d</strong>", s.PaidAmount)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	_, err := uc.WSendGrid.Client.Send(message)
	if err != nil {
		uc.log.WithError(err)
		return err
	}

	return nil
}

func (uc MailingUseCase) NotifyToUserRegistered(s *schema.UserRegisteredSchema) error {
	from := mail.NewEmail(config.DefaultCompanyName, config.DefaultEmailAddressFrom)
	subject := fmt.Sprintf("¡Bienvenid@ %s", s.FullName())
	to := mail.NewEmail(s.FullName(), s.Email)
	plainTextContent := fmt.Sprintf("Te damos la bienvenida a %s", config.DefaultCompanyName)
	htmlContent := fmt.Sprintf("Te damos la bienvenida a %s", config.DefaultCompanyName)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	_, err := uc.WSendGrid.Client.Send(message)
	if err != nil {
		uc.log.WithError(err)
		return err
	}

	return nil
}
