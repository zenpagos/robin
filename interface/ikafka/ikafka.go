package ikafka

import (
	"encoding/json"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/config"
	"gitlab.com/zenpagos/robin/container"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/schema"
	"gitlab.com/zenpagos/robin/wrapper/wkafka"
)

type kafkaProcessor struct {
	log       *logrus.Logger
	container container.Container
}

func NewKafkaProcessor(log *logrus.Logger, c container.Container) *kafkaProcessor {
	return &kafkaProcessor{
		log:       log,
		container: c,
	}
}

func (kp kafkaProcessor) Run() {
	for {
		msg, err := kp.container.WKafka.ReadMessages()
		if err != nil {
			kp.log.WithError(err).Error()
		}

		if msg == nil {
			continue
		}

		// routing by topic
		switch *msg.TopicPartition.Topic {
		case config.PaymentApprovedTopic:
			kp.onPaymentApproved(msg)
		case config.UserRegisteredTopic:
			kp.onUserRegistered(msg)
		}
	}
}

func (kp kafkaProcessor) onUserRegistered(m *wkafka.Message) {
	var msg schema.UserRegisteredSchema

	if err := json.Unmarshal(m.Value, &msg); err != nil {
		kp.log.WithError(err).Error("is not possible to unmarshal the message")
	}

	if err := kp.container.MailingUseCase.NotifyToUserRegistered(&msg); err != nil {
		kp.log.WithError(err).Error("is not possible send the message with the mailer")
	}
}

func (kp kafkaProcessor) onPaymentApproved(m *wkafka.Message) {
	var msg schema.PaymentApprovedSchema

	if err := json.Unmarshal(m.Value, &msg); err != nil {
		kp.log.WithError(err).Error("is not possible to unmarshal the message")
	}

	if err := kp.container.MailingUseCase.NotifyToPayerWhenAPaymentIsReceived(&msg); err != nil {
		kp.log.WithError(err).Error("is not possible send the message with the mailer")
	}

	accountID := uuid.FromStringOrNil(msg.AccountID)
	data := fmt.Sprintf(model.WebhookNotificationDataFormat, msg.PaymentID)

	wn := model.WebhookNotification{
		Type:      model.PaymentType,
		Action:    model.PaymentApprovedAction,
		AccountID: accountID,
		Data:      json.RawMessage(data),
	}
	if err := kp.container.WebhookNotificationUseCase.CreateWebhookNotification(&wn); err != nil {
		kp.log.WithError(err).Error("is not possible create the webhook notification")
	}

	ws, err := kp.container.WebhookUseCase.ListWebhooks(accountID)
	if err != nil {
		kp.log.WithError(err).Error("is not possible get webhooks")
	}

	if err := kp.container.NotificatorUseCase.SendNotification(ws, wn); err != nil {
		kp.log.WithError(err).Error("is not possible create the webhook notification")
	}
}
