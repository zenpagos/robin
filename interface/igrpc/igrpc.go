package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/container"
	"gitlab.com/zenpagos/robin/model"
	"gitlab.com/zenpagos/robin/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type gRPCServer struct {
	log       *logrus.Logger
	container container.Container
}

func NewGRPCServer(log *logrus.Logger, c container.Container) pbv1.RobinServiceServer {
	return &gRPCServer{
		log:       log,
		container: c,
	}
}

func (s *gRPCServer) CreateWebhook(
	c context.Context,
	req *pbv1.CreateWebhookRequest,
) (*pbv1.CreateWebhookResponse, error) {

	w := &model.Webhook{
		Name:                req.Name,
		URL:                 req.URL,
		AuthorizationHeader: tools.StringPointerOrNil(req.AuthorizationHeader),
		AccountID:           uuid.FromStringOrNil(req.AccountID),
	}

	if err := s.container.WebhookUseCase.CreateWebhook(w); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	wr := &pbv1.Webhook{
		ID:                  w.ID.String(),
		CreatedAt:           w.CreatedAt,
		UpdatedAt:           tools.Int64ValueOrZero(w.UpdatedAt),
		Name:                w.Name,
		URL:                 w.URL,
		AuthorizationHeader: tools.StringValueOrBlank(w.AuthorizationHeader),
		AccountID:           w.AccountID.String(),
	}

	res := &pbv1.CreateWebhookResponse{
		Webhook: wr,
	}

	return res, nil
}

func (s *gRPCServer) ListWebhooks(
	c context.Context,
	req *pbv1.ListWebhooksRequest,
) (*pbv1.ListWebhooksResponse, error) {

	accountID := uuid.FromStringOrNil(req.AccountID)

	ws, err := s.container.WebhookUseCase.ListWebhooks(accountID)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	webhooks := make([]*pbv1.Webhook, 0)
	for _, w := range ws {
		webhooks = append(webhooks, &pbv1.Webhook{
			ID:                  w.ID.String(),
			CreatedAt:           w.CreatedAt,
			UpdatedAt:           tools.Int64ValueOrZero(w.UpdatedAt),
			Name:                w.Name,
			URL:                 w.URL,
			AuthorizationHeader: tools.StringValueOrBlank(w.AuthorizationHeader),
			AccountID:           w.AccountID.String(),
		})
	}

	res := &pbv1.ListWebhooksResponse{
		Webhooks: webhooks,
	}

	return res, nil
}
