package container

import (
	"gitlab.com/zenpagos/robin/usecase"
	"gitlab.com/zenpagos/robin/wrapper/wkafka"
)

type Container struct {
	WKafka                     *wkafka.KafkaWrapper
	MailingUseCase             usecase.MailingUseCaseInterface
	WebhookUseCase             usecase.WebhookUseCaseInterface
	WebhookNotificationUseCase usecase.WebhookNotificationUseCaseInterface
	NotificatorUseCase         usecase.NotificatorUseCaseInterface
}

func NewContainer(
	kw *wkafka.KafkaWrapper,
	muc usecase.MailingUseCaseInterface,
	wuc usecase.WebhookUseCaseInterface,
	wnuc usecase.WebhookNotificationUseCaseInterface,
	n usecase.NotificatorUseCaseInterface,
) Container {
	return Container{
		WKafka:                     kw,
		MailingUseCase:             muc,
		WebhookUseCase:             wuc,
		WebhookNotificationUseCase: wnuc,
		NotificatorUseCase:         n,
	}
}
