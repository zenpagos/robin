//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/repository/mysqlrepo"
	"gitlab.com/zenpagos/robin/usecase/mailinguc"
	"gitlab.com/zenpagos/robin/usecase/notificatoruc"
	"gitlab.com/zenpagos/robin/usecase/webhooknotificationuc"
	"gitlab.com/zenpagos/robin/usecase/webhookuc"
	"gitlab.com/zenpagos/robin/wrapper/wkafka"
	"gitlab.com/zenpagos/robin/wrapper/wsendgrid"
)

func InitializeContainer(
	db *gorm.DB,
	log *logrus.Logger,
	kw *wkafka.KafkaWrapper,
	sgw *wsendgrid.SendGridWrapper,
) Container {

	wire.Build(
		// Repositories
		mysqlrepo.NewWebhookRepository,
		mysqlrepo.NewWebhookNotificationRepository,
		// Use Cases
		mailinguc.NewMailingUseCase,
		webhookuc.NewWebhookCase,
		webhooknotificationuc.NewWebhookNotificationCase,
		notificatoruc.NewNotificatorCase,
		NewContainer,
	)

	return Container{}
}
