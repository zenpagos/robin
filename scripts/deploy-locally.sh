#!/bin/bash

SVC_DEPLOYMENT=robin

docker build -t 127.0.0.1:5000/robin .

docker push 127.0.0.1:5000/robin

helm upgrade --install \
  -f scripts/robin-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/robin
