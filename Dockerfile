#
# Use the Go image for building the service
#
FROM golang:1.14-alpine AS builder

# Add dependencies and set the directory
RUN apk add --no-progress --no-cache ca-certificates gcc musl-dev
WORKDIR /go/src/gitlab.com/zenpagos/robin

# Adding the grpc_health_probe
RUN GRPC_HEALTH_PROBE_VERSION=v0.3.2 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# Copy the files needed into the container
COPY . .

# Build the application
RUN GOOS=linux GOARCH=amd64 go build -tags musl -a -installsuffix cgo -o service cmd/main.go


#
# Use the alpine image for running the service
#
FROM alpine:latest

# Download dependencies and set the directory
RUN apk --no-cache add ca-certificates
WORKDIR /root/

# Copy outputs from the builder image
COPY --from=builder /bin/grpc_health_probe ./grpc_health_probe
COPY --from=builder /go/src/gitlab.com/zenpagos/robin/config/config.yaml ./config/config.yaml
COPY --from=builder /go/src/gitlab.com/zenpagos/robin/service ./service

# Run the service application
CMD ["./service"]
