# Nancy

## Kafka and Zookeeper

```shell script
docker service create \
    --env ALLOW_ANONYMOUS_LOGIN=yes \
    --network cenpay \
    --name zookeeper bitnami/zookeeper:3

docker service create \
    --env ALLOW_PLAINTEXT_LISTENER=yes \
    --env KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper:2181 \
    --network cenpay \
    --publish published=9092,target=9092 \
    --name kafka bitnami/kafka:2
```

## Create Topics

```shell script
docker exec -it $(docker ps -q -f name=kafka) kafka-topics.sh \
    --zookeeper zookeeper:2181 --create \
    --partitions 6 --replication-factor 1 \
    --topic notification.payment.approved

docker exec -it $(docker ps -q -f name=kafka) kafka-topics.sh \
    --zookeeper zookeeper:2181 --create \
    --partitions 6 --replication-factor 1 \
    --topic notification.user.registered
```