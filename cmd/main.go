package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/config"
	"gitlab.com/zenpagos/robin/container"
	"gitlab.com/zenpagos/robin/interface/igrpc"
	"gitlab.com/zenpagos/robin/interface/ikafka"
	"gitlab.com/zenpagos/robin/proto/v1"
	"gitlab.com/zenpagos/robin/repository/mysqlrepo"
	"gitlab.com/zenpagos/robin/wrapper/wkafka"
	"gitlab.com/zenpagos/robin/wrapper/wsendgrid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	kw, err := wkafka.NewKafkaWrapper(conf, log)
	if err != nil {
		log.Panicf("error creating kafka wrapper: %v", err)
	}

	sgw, err := wsendgrid.NewSendGridWrapper(conf)
	if err != nil {
		log.Panicf("error creating sendGrid wrapper: %v", err)
	}

	addr := fmt.Sprintf(":%d", conf.Service.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	serviceContainer := container.InitializeContainer(db, log, kw, sgw)

	gRPCServer := grpc.NewServer()

	robinService := igrpc.NewGRPCServer(log, serviceContainer)
	pbv1.RegisterRobinServiceServer(gRPCServer, robinService)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(gRPCServer, healthService)

	kafkaProcessor := ikafka.NewKafkaProcessor(log, serviceContainer)
	go kafkaProcessor.Run()

	if err := gRPCServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}

	defer func() {
		if err := kw.Consumer.Close(); err != nil {
			log.Panicf("error closing the connection: %v", err)
		}
	}()
}
