package wsendgrid

import (
	"github.com/sendgrid/sendgrid-go"
	"gitlab.com/zenpagos/robin/config"
)

type SendGridWrapper struct {
	Client *sendgrid.Client
}

func NewSendGridWrapper(conf config.Configuration) (*SendGridWrapper, error) {
	c := sendgrid.NewSendClient(conf.Service.SendGridApiKey)

	sgw := &SendGridWrapper{
		Client: c,
	}

	return sgw, nil
}
