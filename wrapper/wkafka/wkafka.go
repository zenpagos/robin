package wkafka

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/robin/config"
)

type Message kafka.Message

type KafkaWrapper struct {
	Consumer *kafka.Consumer
}

func NewKafkaWrapper(conf config.Configuration, log *logrus.Logger) (*KafkaWrapper, error) {
	bkrAddr := fmt.Sprintf("%s:%d", conf.Kafka.Host, conf.Kafka.Port)
	kc, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": bkrAddr,
		"group.id":          conf.Kafka.ConsumerGroup,
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		log.WithError(err).Error("error creating kafka consumer")
		return nil, err
	}

	topics := []string{
		config.PaymentApprovedTopic,
		config.UserRegisteredTopic,
	}
	if err := kc.SubscribeTopics(topics, nil); err != nil {
		log.WithError(err).Error("error subscribing to topics")
		return nil, err
	}

	kw := &KafkaWrapper{
		Consumer: kc,
	}

	return kw, nil
}

func (kw KafkaWrapper) ReadMessages() (*Message, error) {
	m, err := kw.Consumer.ReadMessage(-1)
	if err != nil {
		return nil, err
	}

	msg := Message(*m)
	return &msg, err
}
