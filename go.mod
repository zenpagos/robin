module gitlab.com/zenpagos/robin

go 1.14

require (
	github.com/confluentinc/confluent-kafka-go v1.4.1-0.20200429183238-69b368320407
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1
	github.com/go-resty/resty/v2 v2.2.0
	github.com/golang/protobuf v1.3.5
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200427184142-6696b5a3ab05
	github.com/satori/go.uuid v1.2.0
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.6.3
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	google.golang.org/grpc v1.28.1
)
